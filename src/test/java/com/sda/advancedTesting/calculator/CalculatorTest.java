package com.sda.advancedTesting.calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
    @Test
    void verifyAdd(){
        //Given
        double a = 20;
        double b = 32;
        Calculator calculator = new Calculator();
        //When
       double result = calculator.add(a, b);
       //Then
        assertEquals(52, result);


    }
}
